<?php

class UnauthorizedException extends Exception
{
	//
}

class NotFoundException extends Exception
{
	//
}

class InactiveMemberException extends UnauthorizedException
{
	//
}